package com.task20_SpringDataJPA;

import com.task20_SpringDataJPA.EntityTest.TestCity;
import com.task20_SpringDataJPA.EntityTest.TestDiscipline;
import com.task20_SpringDataJPA.EntityTest.TestSchool;
import com.task20_SpringDataJPA.EntityTest.TestStudent;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
    TestStudent.class,
    TestCity.class,
    TestDiscipline.class,
    TestSchool.class
})
public class TestAll {

}
