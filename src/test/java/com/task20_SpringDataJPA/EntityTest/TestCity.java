package com.task20_SpringDataJPA.EntityTest;

import static org.springframework.test.util.AssertionErrors.assertEquals;

import com.task20_SpringDataJPA.Application;
import com.task20_SpringDataJPA.model.City;
import com.task20_SpringDataJPA.repository.CityRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class TestCity {

  @Autowired
  private CityRepository cityRepository;

  @Test
  public void testEntityCity() {
    City mockCity = new City(2, "Kiev");
    City city = cityRepository.findById(2L).get();

    assertEquals("Test id", mockCity.getId(), city.getId());
    assertEquals("Test cityName", mockCity.getName(), city.getName());

  }
}
