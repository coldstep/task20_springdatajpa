package com.task20_SpringDataJPA.EntityTest;

import static org.springframework.test.util.AssertionErrors.assertEquals;

import com.task20_SpringDataJPA.Application;
import com.task20_SpringDataJPA.model.City;
import com.task20_SpringDataJPA.model.School;
import com.task20_SpringDataJPA.model.Student;
import com.task20_SpringDataJPA.repository.StudentRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class TestStudent {


  @Autowired
  private StudentRepository studentRepository;

  private final School mockSchool = new School(
      1, "School №11", "shevchenka", "Mrs.Mackgonagl"
  );
  private final City mockCity = new City(
      2, "Kiev"
  );
  private final Student mockStudent = new Student(
      1, "Zukerberg", "Mark", 21, "zukerberg@gmail.com", mockSchool, mockCity
  );


  @Test()
  public void testEntityStudent() {

    Student student = studentRepository.findById(1L).get();

    assertEquals("Test name", mockStudent.getName(), student.getName());
    assertEquals("Test surname", mockStudent.getSurname(), student.getSurname());
    assertEquals("Test email", mockStudent.getEmail(), student.getEmail());
    assertEquals("Test age", mockStudent.getAge(), student.getAge());
    assertEquals("Test city", mockStudent.getCity(), student.getCity());
    assertEquals("Test  school", mockStudent.getSchool(), student.getSchool());
  }


}
