package com.task20_SpringDataJPA.EntityTest;

import static org.springframework.test.util.AssertionErrors.assertEquals;

import com.task20_SpringDataJPA.Application;
import com.task20_SpringDataJPA.model.School;
import com.task20_SpringDataJPA.repository.SchoolRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class TestSchool {

  @Autowired
  private SchoolRepository schoolRepository;

  @Test
  public void testEntitySchool() {
    School mockSchool = new School(1, "School №11", "shevchenka", "Mrs.Mackgonagl");
    School school = schoolRepository.findById(1L).get();

    assertEquals("Test id ", mockSchool.getId(), school.getId());
    assertEquals("Test name ", mockSchool.getName(), school.getName());
    assertEquals("Test address ", mockSchool.getAddress(), school.getAddress());
    assertEquals("Test director ", mockSchool.getDirector(), school.getDirector());

  }
}
