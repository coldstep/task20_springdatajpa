package com.task20_SpringDataJPA.EntityTest;

import static org.springframework.test.util.AssertionErrors.assertEquals;
import com.task20_SpringDataJPA.Application;
import com.task20_SpringDataJPA.model.Discipline;
import com.task20_SpringDataJPA.repository.DisciplineRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class TestDiscipline {

  @Autowired
  private DisciplineRepository disciplineRepository;

  @Test
  public void testEntityDiscipline() {
    Discipline mockDiscipline = new Discipline(3,"CAD");
    Discipline discipline = disciplineRepository.findById(3L).get();

    assertEquals("Test id ",mockDiscipline.getId(),discipline.getId());
    assertEquals("Test discipline ",mockDiscipline.getDisciplineName(),discipline.getDisciplineName());
  }
}
