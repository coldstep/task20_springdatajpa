package com.task20_SpringDataJPA.DTO;

import com.task20_SpringDataJPA.model.Discipline;

public class DisciplineDTO {

  private long id;
  private String disciplineName;

  public DisciplineDTO() {
  }

  public DisciplineDTO(Discipline discipline) {
    id = discipline.getId();
    disciplineName = discipline.getDisciplineName();
  }

  public Discipline toDiscipline() {
    return new Discipline(id, disciplineName);
  }

  public long getId() {
    return id;
  }

  public String getDisciplineName() {
    return disciplineName;
  }


}
