package com.task20_SpringDataJPA.DTO;

import com.task20_SpringDataJPA.model.City;

public class CityDTO {

  private long id;
  private String name;

  public CityDTO() {
  }

  public CityDTO(City city) {
    id = city.getId();
    name = city.getName();
  }

  public long getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public City toCity() {
    return new City(id, name);
  }
}
