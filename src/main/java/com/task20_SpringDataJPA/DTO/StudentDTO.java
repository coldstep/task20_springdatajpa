package com.task20_SpringDataJPA.DTO;

import com.task20_SpringDataJPA.model.City;
import com.task20_SpringDataJPA.model.Discipline;
import com.task20_SpringDataJPA.model.School;
import com.task20_SpringDataJPA.model.Student;
import java.util.List;
import java.util.stream.Collectors;

public class StudentDTO {

  private long id;
  private String surname;
  private String name;
  private int age;
  private String email;
  private SchoolDTO school;
  private CityDTO city;
  private List<DisciplineDTO> disciplines;

  public StudentDTO() {
  }

  public StudentDTO(Student student) {
    id = student.getId();
    surname = student.getSurname();
    name = student.getName();
    age = student.getAge();
    email = student.getEmail();
    school = new SchoolDTO(student.getSchool());
    city = new CityDTO(student.getCity());
    disciplines = student.getDisciplines().stream().map(DisciplineDTO::new)
        .collect(Collectors.toList());

  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getAge() {
    return age;
  }

  public void setAge(int age) {
    this.age = age;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public SchoolDTO getSchool() {
    return school;
  }

  public void setSchool(SchoolDTO school) {
    this.school = school;
  }

  public CityDTO getCity() {
    return city;
  }

  public void setCity(CityDTO city) {
    this.city = city;
  }

  public List<DisciplineDTO> getDisciplines() {
    return disciplines;
  }

  public void setDisciplines(List<DisciplineDTO> disciplines) {
    this.disciplines = disciplines;
  }

  public Student toStudent() {
    List<Discipline> tempList = disciplines.stream()
        .map(a -> new Discipline(a.getId(), a.getDisciplineName()))
        .collect(Collectors.toList());
    School tempSchool = new School(
        school.getId(),
        school.getName(),
        school.getAddress(),
        school.getDirector()
    );
    City tempCity = new City(
        city.getId(),
        city.getName()
    );
    return new Student(
        id,
        surname,
        name,
        age,
        email,
        tempSchool,
        tempCity,
        tempList
    );
  }
}
