package com.task20_SpringDataJPA.DTO;

import com.task20_SpringDataJPA.model.School;

public class SchoolDTO {

  private long id;
  private String name;
  private String address;
  private String director;

  public SchoolDTO() {
  }

  public SchoolDTO(String name, String director) {
    this.name = name;
    this.director = director;
  }

  public SchoolDTO(School school) {
    id = school.getId();
    name = school.getName();
    address = school.getAddress();
    director = school.getDirector();
  }


  public long getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public String getAddress() {
    return address;
  }

  public String getDirector() {
    return director;
  }

  public School toSchool() {
    return new School(id, name, address, director);
  }
}
