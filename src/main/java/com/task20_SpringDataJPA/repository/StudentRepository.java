package com.task20_SpringDataJPA.repository;

import com.task20_SpringDataJPA.model.Student;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {

  List<Student> findByName(String city);

  @Query(value = "select * from student where city_id in (select id from city where name = :cityName)", nativeQuery = true)
  List<Student> findByCity(@Param("cityName") String name);

}
