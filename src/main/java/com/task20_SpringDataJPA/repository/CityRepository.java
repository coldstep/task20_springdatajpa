package com.task20_SpringDataJPA.repository;

import com.task20_SpringDataJPA.model.City;
import java.util.List;
import org.hibernate.sql.Select;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface CityRepository extends JpaRepository<City, Long> {

  City findByName(String name);
}
