package com.task20_SpringDataJPA.repository;

import com.task20_SpringDataJPA.model.School;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface SchoolRepository extends JpaRepository<School, Long> {

  @Query( "select name , director from School ")
  List<School> findNameAndDirector();

}
