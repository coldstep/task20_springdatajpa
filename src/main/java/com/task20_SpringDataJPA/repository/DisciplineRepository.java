package com.task20_SpringDataJPA.repository;

import com.task20_SpringDataJPA.model.Discipline;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DisciplineRepository extends JpaRepository<Discipline,Long> {

}
