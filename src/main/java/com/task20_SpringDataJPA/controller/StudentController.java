package com.task20_SpringDataJPA.controller;

import com.task20_SpringDataJPA.DTO.StudentDTO;
import com.task20_SpringDataJPA.service.StudentService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/student")
public class StudentController {

  @Autowired
  private StudentService studentService;

  @GetMapping("/getAllStudents")
  public List<StudentDTO> getAll() {
    return studentService.getAll();
  }

  @GetMapping("/getByCity")
  public List<StudentDTO> getByCity(@RequestParam String city) {
    return studentService.getByCity(city);
  }

  @GetMapping("/sortByCity")
  public List<StudentDTO> sortByCity() {
    return studentService.sortByCity();
  }

  @DeleteMapping("/deleteById")
  public void deleteById(@RequestParam long id) {
    studentService.deleteByID(id);
  }

  @PutMapping("/update")
  public void update(@RequestBody StudentDTO studentDTO) {
    studentService.update(studentDTO);
  }

  @PostMapping("/insert")
  public void insert(@RequestBody StudentDTO studentDTO) {
    studentService.insert(studentDTO);
  }
}
