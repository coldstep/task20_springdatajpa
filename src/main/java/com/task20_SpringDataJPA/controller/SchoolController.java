package com.task20_SpringDataJPA.controller;

import com.task20_SpringDataJPA.DTO.SchoolDTO;

import com.task20_SpringDataJPA.model.School;
import com.task20_SpringDataJPA.service.SchoolService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/school")
public class SchoolController {


  @Autowired
  private SchoolService schoolService;

  @GetMapping("/getAllSchools")
  public List<SchoolDTO> getAll() {
    return schoolService.getAll();
  }

  @GetMapping("/getAllSchoolAndDirectory")
  public List<School> getAllSchoolAndDirectory() {
    return schoolService.getAllSchoolAndDirectory();
  }

  @GetMapping("/getByID")
  public SchoolDTO getByID(@RequestParam long id) {
    return schoolService.getByID(id);
  }

  @DeleteMapping("/deleteById")
  public void deleteById(@RequestParam long id) {
    schoolService.deleteByID(id);
  }

  @PutMapping("/update")
  public void update(@RequestBody SchoolDTO schoolDTO) {
    schoolService.update(schoolDTO);
  }

  @PostMapping("/insert")
  public void insert(@RequestBody SchoolDTO schoolDTO) {
    schoolService.insert(schoolDTO);
  }

}
