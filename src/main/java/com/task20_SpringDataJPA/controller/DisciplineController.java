package com.task20_SpringDataJPA.controller;

import com.task20_SpringDataJPA.DTO.DisciplineDTO;
import com.task20_SpringDataJPA.service.DisciplineService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/discipline")
public class DisciplineController {

  @Autowired
  private DisciplineService disciplineService;

  @GetMapping("/getAllDiscipline")
  public List<DisciplineDTO> getAll() {
    return disciplineService.getAll();
  }

  @GetMapping("/getByID")
  public DisciplineDTO getById(@RequestParam long id) {
    return disciplineService.getByID(id);
  }

  @DeleteMapping("/deleteById")
  public void deleteById(@RequestParam long id) {
    disciplineService.deleteByID(id);
  }

  @PutMapping("/update")
  public void update(@RequestBody DisciplineDTO disciplineDTO) {
    disciplineService.update(disciplineDTO);
  }

  @PostMapping("/insert")
  public void insert(@RequestBody DisciplineDTO disciplineDTO) {
    disciplineService.insert(disciplineDTO);
  }

  @GetMapping("/sortByDiscipline")
  public List<DisciplineDTO> sort() {
    return disciplineService.sortByDiscipline();
  }
}
