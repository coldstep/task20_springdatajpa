package com.task20_SpringDataJPA.controller;

import com.task20_SpringDataJPA.DTO.CityDTO;
import com.task20_SpringDataJPA.service.CityService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/city")
public class CityController {

  @Autowired
  private CityService cityService;

  @GetMapping("/getAll")
  public List<CityDTO> getAll() {
    return cityService.getAll();
  }

  @GetMapping("/getById")
  public CityDTO getByID(@RequestParam long id) {
    return cityService.getByID(id);
  }

  @GetMapping("/sortByCity")
  public List<CityDTO> sortByCity() {
    return cityService.getAll();
  }

  @GetMapping("/gerByCity")
  public CityDTO getByCity(@RequestParam String city) {
    return cityService.getByCity(city);
  }

  @DeleteMapping("/deleteByID")
  public void deleteByID(@RequestParam long id) {
    cityService.deleteByID(id);
  }

  @PutMapping("/update")
  public void update(@RequestBody CityDTO cityDTO) {
    cityService.update(cityDTO);
  }

  @PostMapping("/insert")
  public void insert(@RequestBody CityDTO cityDTO) {
    cityService.insert(cityDTO);
  }

}
