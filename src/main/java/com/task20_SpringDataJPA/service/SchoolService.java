package com.task20_SpringDataJPA.service;

import com.task20_SpringDataJPA.DTO.SchoolDTO;
import com.task20_SpringDataJPA.model.School;
import java.util.List;

public interface SchoolService {

  List<SchoolDTO> getAll();

  SchoolDTO getByID(long id);

  List<School> getAllSchoolAndDirectory();

  void deleteByID(long id);

  void update(SchoolDTO cityDTO);

  void insert(SchoolDTO cityDTO);

}
