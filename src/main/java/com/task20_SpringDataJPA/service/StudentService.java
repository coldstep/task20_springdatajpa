package com.task20_SpringDataJPA.service;

import com.task20_SpringDataJPA.DTO.StudentDTO;

import java.util.List;


public interface StudentService {

  List<StudentDTO> getAll();

  StudentDTO getByID(long id);

  List<StudentDTO> getByCity(String city);

  List<StudentDTO> sortByCity();

  void deleteByID(long id);

  void update(StudentDTO studentDTO);

  void insert(StudentDTO studentDTO);

}
