package com.task20_SpringDataJPA.service.impl;

import com.task20_SpringDataJPA.DTO.StudentDTO;
import com.task20_SpringDataJPA.model.Student;
import com.task20_SpringDataJPA.repository.StudentRepository;
import com.task20_SpringDataJPA.service.StudentService;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class StudentServiceImpl implements StudentService {

  @Autowired
  private StudentRepository studentRepository;

  @Override
  public List<StudentDTO> getAll() {
    return studentRepository.findAll()
        .stream()
        .map(StudentDTO::new)
        .collect(Collectors.toList());
  }

  @Override
  public StudentDTO getByID(long id) {
    return new StudentDTO(studentRepository.findById(id)
        .get());
  }

  @Override
  public List<StudentDTO> getByCity(String city) {
    return studentRepository.findByCity(city)
        .stream()
        .map(StudentDTO::new)
        .collect(Collectors.toList());
  }

  @Override
  public List<StudentDTO> sortByCity() {
    return studentRepository.findAll(Sort.by("city"))
        .stream()
        .map(StudentDTO::new)
        .collect(Collectors.toList());
  }

  @Override
  public void deleteByID(long id) {
    studentRepository.deleteById(id);
  }

  @Override
  public void update(StudentDTO studentDTO) {
    studentRepository.save(studentDTO.toStudent());
  }

  @Override
  public void insert(StudentDTO studentDTO) {
    studentRepository.save(studentDTO.toStudent());
  }
}
