package com.task20_SpringDataJPA.service.impl;

import com.task20_SpringDataJPA.DTO.SchoolDTO;
import com.task20_SpringDataJPA.model.School;
import com.task20_SpringDataJPA.repository.SchoolRepository;
import com.task20_SpringDataJPA.service.SchoolService;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SchoolServiceImpl implements SchoolService {

  @Autowired
  private SchoolRepository schoolRepository;

  @Override
  public List<SchoolDTO> getAll() {
    return schoolRepository.findAll()
        .stream()
        .map(SchoolDTO::new)
        .collect(Collectors.toList());
  }

  @Override
  public SchoolDTO getByID(long id) {
    return new SchoolDTO(
        schoolRepository.findById(id)
            .get()
    );
  }

  @Override
  public List<School> getAllSchoolAndDirectory() {
    return schoolRepository.findNameAndDirector();
  }

  @Override
  public void deleteByID(long id) {
    schoolRepository.deleteById(id);
  }

  @Override
  public void update(SchoolDTO schoolDTO) {
    schoolRepository.save(schoolDTO.toSchool());
  }

  @Override
  public void insert(SchoolDTO schoolDTO) {
    schoolRepository.save(schoolDTO.toSchool());

  }


}
