package com.task20_SpringDataJPA.service.impl;

import com.task20_SpringDataJPA.DTO.CityDTO;
import com.task20_SpringDataJPA.repository.CityRepository;
import com.task20_SpringDataJPA.service.CityService;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class CityServiceImpl implements CityService {

  @Autowired
  CityRepository cityRepository;

  @Override
  public List<CityDTO> getAll() {
    return cityRepository.findAll()
        .stream()
        .map(CityDTO::new)
        .collect(Collectors.toList());
  }

  @Override
  public CityDTO getByID(long id) {
    return new CityDTO(cityRepository.findById(id).get());
  }

  @Override
  public CityDTO getByCity(String city) {
    return new CityDTO(cityRepository.findByName(city));
  }

  @Override
  public List<CityDTO> sortByCity() {
    return cityRepository.findAll(Sort.by("name"))
        .stream()
        .map(CityDTO::new)
        .collect(Collectors.toList());
  }

  @Override
  public void deleteByID(long id) {
    cityRepository.deleteById(id);
  }


  @Override
  public void update(CityDTO cityDTO) {
    cityRepository.save(cityDTO.toCity());
  }

  @Override
  public void insert(CityDTO cityDTO) {
    cityRepository.save(cityDTO.toCity());
  }
}
