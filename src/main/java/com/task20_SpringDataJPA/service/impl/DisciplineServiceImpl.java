package com.task20_SpringDataJPA.service.impl;

import com.task20_SpringDataJPA.DTO.DisciplineDTO;
import com.task20_SpringDataJPA.repository.DisciplineRepository;
import com.task20_SpringDataJPA.service.DisciplineService;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class DisciplineServiceImpl implements DisciplineService {

  @Autowired
  private DisciplineRepository disciplineRepository;

  @Override
  public List<DisciplineDTO> getAll() {
    return disciplineRepository.findAll().stream().map(DisciplineDTO::new)
        .collect(Collectors.toList());
  }

  @Override
  public DisciplineDTO getByID(long id) {
    return new DisciplineDTO(disciplineRepository.findById(id).get());
  }

  @Override
  public List<DisciplineDTO> sortByDiscipline() {
    return disciplineRepository.findAll(Sort.by("discipline"))
        .stream()
        .map(DisciplineDTO::new)
        .collect(Collectors.toList());
  }

  @Override
  public void deleteByID(long id) {
    disciplineRepository.deleteById(id);
  }

  @Override
  public void update(DisciplineDTO disciplineDTO) {
    disciplineRepository.save(disciplineDTO.toDiscipline());

  }

  @Override
  public void insert(DisciplineDTO disciplineDTO) {
    disciplineRepository.save(disciplineDTO.toDiscipline());

  }


}
