package com.task20_SpringDataJPA.service;

import com.task20_SpringDataJPA.DTO.CityDTO;
import java.util.List;

public interface CityService {

  List<CityDTO> getAll();

  CityDTO getByID(long id);

  CityDTO getByCity(String city);

  List<CityDTO> sortByCity();

  void deleteByID(long id);

  void update(CityDTO cityDTO);

  void insert(CityDTO cityDTO);

}
