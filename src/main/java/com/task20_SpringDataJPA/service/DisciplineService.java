package com.task20_SpringDataJPA.service;

import com.task20_SpringDataJPA.DTO.DisciplineDTO;
import java.util.List;

public interface DisciplineService {

  List<DisciplineDTO> getAll();

  DisciplineDTO getByID(long id);

  List<DisciplineDTO> sortByDiscipline();

  void deleteByID(long id);

  void update(DisciplineDTO disciplineDTO);

  void insert(DisciplineDTO disciplineDTO);

}
