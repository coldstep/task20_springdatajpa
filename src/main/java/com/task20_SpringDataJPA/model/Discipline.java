package com.task20_SpringDataJPA.model;

import java.util.List;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "discipline", schema = "epam_homework3")
public class Discipline {

  @Id
  @Column(name = "id")
  private long id;

  @Column(name = "discipline_name")
  private String disciplineName;

//  @ManyToMany(mappedBy = "disciplines",fetch = FetchType.EAGER) - for test
  @ManyToMany(mappedBy = "disciplines")
  private List<Student> students;


  public Discipline() {
  }


  public Discipline(long id) {
    this.id = id;
  }

  public Discipline(String disciplineName) {
    this.disciplineName = disciplineName;
  }

  public Discipline(long id, String disciplineName) {
    this.id = id;
    this.disciplineName = disciplineName;
  }

  public Discipline(long id, String disciplineName,
      List<Student> students) {
    this.id = id;
    this.disciplineName = disciplineName;
    this.students = students;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getDisciplineName() {
    return disciplineName;
  }

  public List<Student> getStudents() {
    return students;
  }

  public void setStudents(List<Student> students) {
    this.students = students;
  }

  public void setDisciplineName(String disciplineName) {
    this.disciplineName = disciplineName;
  }


  @Override
  public String toString() {
    return "Discipline{" +
        "id=" + id +
        ", disciplineName='" + disciplineName + '\'' +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Discipline)) {
      return false;
    }
    Discipline that = (Discipline) o;
    return id == that.id &&
        disciplineName.equals(that.disciplineName);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, disciplineName);
  }
}
