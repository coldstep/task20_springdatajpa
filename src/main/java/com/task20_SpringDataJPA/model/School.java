package com.task20_SpringDataJPA.model;

import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "school", schema = "epam_homework3")
public class School {

  @Id
  @Column(name = "id")
  private long id;

  @Column(name = "name")
  private String name;

  @Column(name = "address")
  private String address;

  @Column(name = "director")
  private String director;

  public School() {
  }

  public School(long id) {
    this.id = id;
  }

  public School(String name) {
    this.name = name;
  }

  public School(String name, String director) {
    this.name = name;
    this.director = director;
  }

  public School(long id, String name, String address, String director) {
    this.id = id;
    this.name = name;
    this.address = address;
    this.director = director;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getDirector() {
    return director;
  }

  public void setDirector(String director) {
    this.director = director;
  }

  @Override
  public String toString() {
    return "School{" +
        "id=" + id +
        ", name='" + name + '\'' +
        ", address='" + address + '\'' +
        ", director='" + director + '\'' +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof School)) {
      return false;
    }
    School school = (School) o;
    return id == school.id &&
        name.equals(school.name) &&
        Objects.equals(address, school.address) &&
        director.equals(school.director);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, name, address, director);
  }
}
