package com.task20_SpringDataJPA.model;

import java.util.List;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "student", schema = "epam_homework3)")
public class Student {

  @Id
  @Column(name = "id")
  private long id;
  @Column(name = "surname")
  private String surname;
  @Column(name = "name")
  private String name;
  @Column(name = "age")
  private int age;
  @Column(name = "email")
  private String email;
  @ManyToOne
  @JoinColumn(name = "school_id", referencedColumnName = "id", nullable = false)
  private School school;
  @ManyToOne
  @JoinColumn(name = "city_id", referencedColumnName = "id", nullable = false)
  private City city;


//  @ManyToMany(fetch = FetchType.EAGER) - for test
  @ManyToMany
  @JoinTable(name = "student_has_discipline",
      joinColumns = @JoinColumn(name = "student_id", referencedColumnName = "id", nullable = false),
      inverseJoinColumns = @JoinColumn(name = "discipline_id", referencedColumnName = "id", nullable = false))
  private List<Discipline> disciplines;

  public Student() {
  }

  public Student(long id) {
    this.id = id;
  }

  public Student(long id, String surname, String name, int age, String email,
      School school, City city) {
    this.id = id;
    this.surname = surname;
    this.name = name;
    this.age = age;
    this.email = email;
    this.school = school;
    this.city = city;
  }

  public Student(long id, String surname, String name, int age, String email,
      School school, City city, List<Discipline> disciplines) {
    this.id = id;
    this.surname = surname;
    this.name = name;
    this.age = age;
    this.email = email;
    this.school = school;
    this.city = city;
    this.disciplines = disciplines;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getAge() {
    return age;
  }

  public void setAge(int age) {
    this.age = age;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public School getSchool() {
    return school;
  }

  public void setSchool(School school) {
    this.school = school;
  }

  public City getCity() {
    return city;
  }

  public void setCity(City city) {
    this.city = city;
  }

  public List<Discipline> getDisciplines() {
    return disciplines;
  }

  public void setDisciplines(List<Discipline> disciplines) {
    this.disciplines = disciplines;
  }

  @Override
  public String toString() {
    return "Student{" +
        "id=" + id +
        ", surname='" + surname + '\'' +
        ", name='" + name + '\'' +
        ", age=" + age +
        ", email='" + email + '\'' +
        ", school=" + school +
        ", city=" + city +
        ", disciplines=" + disciplines +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Student)) {
      return false;
    }
    Student student = (Student) o;
    return id == student.id &&
        age == student.age &&
        surname.equals(student.surname) &&
        name.equals(student.name) &&
        email.equals(student.email) &&
        Objects.equals(school, student.school) &&
        Objects.equals(city, student.city) &&
        Objects.equals(disciplines, student.disciplines);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, surname, name, age, email, school, city, disciplines);
  }
}
