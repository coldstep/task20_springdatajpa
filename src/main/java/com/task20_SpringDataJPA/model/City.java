package com.task20_SpringDataJPA.model;

import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "city", schema = "epam_homework3")
public class City {

  @Id
  @Column(name = "id")
  private long id;

  @Column(name = "name")
  private String name;

  public City() {
  }

  public City(long id) {
    this.id = id;
  }

  public City(String name) {
    this.name = name;
  }

  public City(long id, String name) {
    this.id = id;
    this.name = name;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public String toString() {
    return "City{" +
        "id=" + id +
        ", name='" + name + '\'' +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof City)) {
      return false;
    }
    City city = (City) o;
    return id == city.id &&
        name.equals(city.name);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, name);
  }
}
