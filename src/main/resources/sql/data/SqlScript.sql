INSERT INTO discipline (discipline_name)
VALUES ('Information technology'),
	('Line algebra'),
    ('CAD'),
    ('Operation system'),
    ('Philosophy');


INSERT INTO student
		(
		  id,
			surname,
			name,
			age,
			email,
			school_id,
			city_id
		)
VALUES
	(1,'Zukerberg','Mark',21,'zukerberg@gmail.com',1,2),
	(2,'Gates','Bill',22,'bill@gmail.com',2,1),
    (3,'Stark','Tony',30,'stark@gmail.com',3,4),
    (4,'Klatte','Susanne',22,'klat@gmail.com',1,3),
    (5,'Gosling','James',30,'james@gmail.com',2,2)
;

INSERT INTO city(name)
VALUES
	('Lviv'),
    ('Kiev'),
    ('Chernigiv'),
    ('Odessa'),
    ('Kirovograd');

INSERT INTO school
VALUES
	(1,'School №11','shevchenka','Mrs.Mackgonagl')
    ,(2,'School №44','chornovola','Mr.Rude')
    ,(3,'School №56','zelena','Mr.Chan')
    ,(4,'School №53','syhiv','Mrs.Smith')
    ,(5,'School №33','gorodocka','Mr.BackHold');

INSERT INTO `epam_homework3`.`student_has_discipline` (`student_id`, `discipline_id`) VALUES ('1', '2');
INSERT INTO `epam_homework3`.`student_has_discipline` (`student_id`, `discipline_id`) VALUES ('3', '4');
INSERT INTO `epam_homework3`.`student_has_discipline` (`student_id`, `discipline_id`) VALUES ('3', '2');
INSERT INTO `epam_homework3`.`student_has_discipline` (`student_id`, `discipline_id`) VALUES ('2', '3');