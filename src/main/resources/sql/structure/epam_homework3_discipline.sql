CREATE DATABASE  IF NOT EXISTS `epam_homework3` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `epam_homework3`;

 SET NAMES utf8 ;

--
-- Table structure for table `discipline`
--

DROP TABLE IF EXISTS `discipline`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `discipline` (
  `id` bigint(11) NOT NULL,
  `discipline_name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
