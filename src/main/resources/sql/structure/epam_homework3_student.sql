CREATE DATABASE  IF NOT EXISTS `epam_homework3` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `epam_homework3`;

 SET NAMES utf8 ;

--
-- Table structure for table `student`
--

DROP TABLE IF EXISTS `student`;

 SET character_set_client = utf8mb4 ;
CREATE TABLE `student` (
  `id` bigint(11) NOT NULL,
  `surname` varchar(45) NOT NULL,
  `name` varchar(45) NOT NULL,
  `age` int(11) NOT NULL,
  `email` varchar(45) NOT NULL,
  `school_id` bigint(11) NOT NULL,
  `city_id` bigint(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_student_school1_idx` (`school_id`),
  KEY `fk_student_city1_idx` (`city_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
