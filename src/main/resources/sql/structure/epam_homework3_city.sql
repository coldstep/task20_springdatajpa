CREATE DATABASE  IF NOT EXISTS `epam_homework3` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `epam_homework3`;

 SET NAMES utf8 ;

--
-- Table structure for table `city`
--

DROP TABLE IF EXISTS `city`;

 SET character_set_client = utf8mb4 ;
CREATE TABLE `city` (
  `id` bigint(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
