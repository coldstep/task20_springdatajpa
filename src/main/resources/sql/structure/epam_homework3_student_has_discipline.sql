CREATE DATABASE  IF NOT EXISTS `epam_homework3` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `epam_homework3`;

 SET NAMES utf8 ;

--
-- Table structure for table `student_has_discipline`
--

DROP TABLE IF EXISTS `student_has_discipline`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `student_has_discipline` (
  `discipline_id` bigint(20) NOT NULL,
  `student_id` bigint(20) NOT NULL,
  KEY `FKr974wcjs8ufrgd99k22kh6xgy` (`discipline_id`),
  KEY `FKhpd2jft6ieiykw8688hy7sjrm` (`student_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
